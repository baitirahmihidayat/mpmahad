<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SISTEM PERIZINAN MAHAD</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/stylish-portfolio.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <!-- Navigation -->
    <a id="menu-toggle" href="#" class="btn btn-dark btn-lg toggle"><i class="fa fa-bars"></i></a>
    <nav id="sidebar-wrapper">
        <ul class="sidebar-nav">
            <a id="menu-close" href="#" class="btn btn-light btn-lg pull-right toggle"><i class="fa fa-times"></i></a>
            <li class="sidebar-brand">
            
                <a href="#top" onclick=$("#menu-close").click();>Home</a>
            </li>
            <li>
                <a href="#about" onclick=$("#menu-close").click();>About</a>
            </li>
            <li>
                <a href="#contact" onclick=$("#menu-close").click();>Contact</a>
            </li>
        </ul>
    </nav>

    <!-- Header -->
    <section id="top" class="header">
        <div class="text-vertical-center">
            <h1 style="color: black;">Sistem Perizinan Ma'had Sunan Ampel Al - Aly</h1>
            <br>
            <button class="btn btn-primary" onclick="location.href='http://localhost/mahad/login.php';">Login</button>
        </div>
    </section>


    
              <!--       <h2>Profil Ma'had</h2>
                    <p class="lead"> Dalam pandangan Islam, mahasiswa merupakan komunitas yang terhormat dan terpuji karena mereka merupakan cikal bakal lahirnya ilmuwan (ulama’) yang diharapkan mampu mengembangkan ilmu<a target="_blank"
                </div>
            </div>
        </div> -->
        <!-- About -->
    <section id="about" class="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2>Profil Ma'had</h2>
                    <p class="lead">Dalam pandangan Islam, mahasiswa merupakan komunitas yang terhormat dan terpuji karena mereka merupakan cikal bakal lahirnya ilmuwan (ulama’) yang diharapkan mampu mengembangkan ilmu</p>
                </div>
            </div>
        </div

        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2>Visi Ma'had</h2>
                    <p class="lead">Terwujudnya pusat pemantapan akidah, pengembangan ilmu keislaman, amal shalih, akhlak mulia, pusat informasi pesantren dan sebagai sendi terciptanya masyarakat muslim Indonesia yang cerdas, dinamis, kreatif, damai dan sejahtera</p>
                </div>
            </div>
        </div
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2>Misi Ma'had</h2>
                    <p class="lead">1. Mengantarkan mahasiswa memiliki kemantapan akidah dan kedalaman spiritual, keluhuran akhlak, keluasan ilmu dan kematangan professional</p>
                    <p class="lead">2. Memberikan ketrampilan berbahasa Arab dan Inggris</p>
                    <p class="lead">3. Memperdalam bacaan dan makna al-Qur’an dengan benar dan baik</p>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </section>

    
    

    <!-- Portfolio -->
    <section id="galeri" class="galeri">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 col-lg-offset-1 text-center">
                    
                    <h2>Galeri Ma'had</h2>
                            <div class="galeri-item">
                            <div class="col-md-12">
                            <div class="galeri-item">
                                    <img class="img-galeri img-responsive" src="img/uin1.jpg">
                            </div>
                            <br>
                        <div class="col-md-6">
                            <div class="galeri-item">
                                    <img class="img-portfolio img-responsive" src="img/uin4.jpg">
                            </div>
                        </div>
                        <br>
                        <div class="col-md-6">
                            <div class="portfolio-item">
                                    <img class="img-portfolio img-responsive" src="img/uin6.jpg">
                            </div>
                        </div>
                    </div>
                    </section>
            

             <!--    <section id="login" class="login">
                    <div class="container">
                    <form>
                        <div class="col-md-6 text-center">
                            <input type="text" id="username" placeholder="username">
                        </div>
                        <div class="col-md-6 text-center">
                            <input type="password" id="password" placeholder="password">
                        </div>
                    </form>
                    </div>
                </section>
 -->
    <!-- Map -->
<!--     <section id="contact" class="map">
        <iframe width="100%" height="100%" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.co.id/maps/place/UIN+Maulana+Malik+Ibrahim+Malang/@-7.9499676,112.6050571,17z/data=!4m8!1m2!2m1!1sma'had+uin+malang!3m4!1s0x2e78815e45bbc56d:0x5031ab2d7d36dba6!8m2!3d-7.9508579!4d112.608149?hl=id"></iframe>
        <br />
        <small>
            <a href="https://www.google.co.id/maps/place/UIN+Maulana+Malik+Ibrahim+Malang/@-7.9499676,112.6050571,17z/data=!4m8!1m2!2m1!1sma'had+uin+malang!3m4!1s0x2e78815e45bbc56d:0x5031ab2d7d36dba6!8m2!3d-7.9508579!4d112.608149?hl=id"></a>
        </small>
        </iframe>
    </section> -->

    <!-- Footer -->
    <footer>
    <section id="contact" class="Contact">
        <div class="container">
            <div class="row">
                <div class="text-center">
                    <h4><center>Hubungi Kami</center></h4>
                    <br>
                    <h4><center>Jl. Gajayana No. 50, Dinoyo, Kec. Lowokwaru, Malang</center></h4>
                    <br>
                    <h4><center>Jawa Timur 65149</center></h4>
                    <ul class="list-unstyled">
                        <li><i class="fa fa-phone fa-fw"></i> (0341) 551354</li>
                        <li><i class="fa fa-envelope-o fa-fw"></i> <a href="mailto:name@example.com">uin-malang.ac.id</a>
                        </li>
                    </ul>
                    <br>
                    <ul class="list-inline">
                        <li>
                            <a href="#"><i class="fa fa-facebook fa-fw fa-3x"></i></a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-twitter fa-fw fa-3x"></i></a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-dribbble fa-fw fa-3x"></i></a>
                        </li>
                    </ul>
                    <hr class="small">
                    <p class="text-muted">Copyright &copy; Ma'had Sunan Ampel Al-Aly</p>
                </div>
                </div>
            </div>
        </div>
        <a id="to-top" href="#top" class="btn btn-dark btn-lg"><i class="fa fa-chevron-up fa-fw fa-1x"></i></a>
    </footer>

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script>
    // Closes the sidebar menu
    $("#menu-close").click(function(e) {
        e.preventDefault();
        $("#sidebar-wrapper").toggleClass("active");
    });
    // Opens the sidebar menu
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#sidebar-wrapper").toggleClass("active");
    });
    // Scrolls to the selected menu item on the page
    $(function() {
        $('a[href*=#]:not([href=#],[data-toggle],[data-target],[data-slide])').click(function() {
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') || location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    $('html,body').animate({
                        scrollTop: target.offset().top
                    }, 1000);
                    return false;
                }
            }
        });
    });
    //#to-top button appears after scrolling
    var fixed = false;
    $(document).scroll(function() {
        if ($(this).scrollTop() > 250) {
            if (!fixed) {
                fixed = true;
                // $('#to-top').css({position:'fixed', display:'block'});
                $('#to-top').show("slow", function() {
                    $('#to-top').css({
                        position: 'fixed',
                        display: 'block'
                    });
                });
            }
        } else {
            if (fixed) {
                fixed = false;
                $('#to-top').hide("slow", function() {
                    $('#to-top').css({
                        display: 'none'
                    });
                });
            }
        }
    });
    // Disable Google Maps scrolling
    // See http://stackoverflow.com/a/25904582/1607849
    // Disable scroll zooming and bind back the click event
    var onMapMouseleaveHandler = function(event) {
        var that = $(this);
        that.on('click', onMapClickHandler);
        that.off('mouseleave', onMapMouseleaveHandler);
        that.find('iframe').css("pointer-events", "none");
    }
    var onMapClickHandler = function(event) {
            var that = $(this);
            // Disable the click handler until the user leaves the map area
            that.off('click', onMapClickHandler);
            // Enable scrolling zoom
            that.find('iframe').css("pointer-events", "auto");
            // Handle the mouse leave event
            that.on('mouseleave', onMapMouseleaveHandler);
        }
        // Enable map zooming with mouse scroll when the user clicks the map
    $('.map').on('click', onMapClickHandler);
    </script>

</body>

</html>
