<!DOCTYPE html>
<html>
<head>
	<title>SISTEM PERIZINAN</title>
	<!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/stylish-portfolio.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body class="header">
	 <div class="Absolute-Center is-Responsive">
                <br>
                <br>
                <br>
                <div class="col-sm-12">
                    <form action="" id="loginform">
                        <div class="form-group">
                            <input type="text" id="username" class="form-control" placeholder="username">
                        </div>
                        <div class="form-group">
                            <input type="password" id="password" class="form-control" placeholder="password">
                        </div>
                        <div class="form-group">
                            <center><button type="submit" class="btn btn-primary">Login</button></center>
                        </div>
                    </form>
                </div>

            </div>
</body>
</html>