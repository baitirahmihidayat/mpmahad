<!DOCTYPE html>
<html>
<head>
	<title>SISTEM PERIZINAN</title>
	<!-- Bootstrap Core CSS --> 
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/stylish-portfolio.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
</head>
<body class="header">
            <section class="content">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="text-muted">Form Perizinan Ma'had</h1>
                        <div class="panel panel-primary">
                            <form class="form-horizontal" action="../../controllers/productController.php" method="post" enctype="multipart/form-data">
                                <div class="panel-body">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Nomor</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" id="nomor" name="Nomor" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Nama Kegiatan</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" id="nama_kegiatan" name="nama_kegiatan" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Tempat Kegiatan</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" name="tempat_kegiatan" id="tempat_kegiatan" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Instansi</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" name="instansi" id="instansi" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Nama Penanggung Jawab</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" name="pj" id="pj" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Nomor HP</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" name="no_hp" id="no_hp" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Tanggal Awal</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" name="tgl_awal" id="tgl_awal" required>
                                            </div>
                                        </div>
                                <div class="panel-footer" align="right">
                                    <button type="button" class="btn btn-danger" id="cancelSubmit">Batal</button>
                                    <button type="button" class="btn btn-success" id="resetSubmit">Reset</button>
                                    <button type="submit" name="submitProduct" class="btn btn-primary">Simpan</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>